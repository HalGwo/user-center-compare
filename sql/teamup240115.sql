/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : teamup

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 15/01/2024 18:47:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `tagName` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签名称',
  `userId` bigint(0) NULL DEFAULT NULL COMMENT '用户 id',
  `parentId` bigint(0) NULL DEFAULT NULL COMMENT '父标签 id',
  `isParent` tinyint(0) NULL DEFAULT NULL COMMENT '0 - 不是, 1 - 父标签',
  `createTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `isDelete` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniIdx_tagName`(`tagName`) USING BTREE,
  INDEX `idx_userId`(`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `userAccount` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `avatarUrl` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `gender` tinyint(0) NULL DEFAULT NULL COMMENT '性别',
  `userPassword` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `email` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `profile` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人简介',
  `userStatus` int(0) NULL DEFAULT 0 COMMENT '状态 0-正常',
  `phone` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `createTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `isDelete` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `planetCode` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '星球编号',
  `userRole` int(0) NOT NULL DEFAULT 0 COMMENT '用户角色 0 - 普通用户 1 - 管理员',
  `tags` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签列表 json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'HalGwo', 'HalGwo', 'https://avatars.githubusercontent.com/u/55083373', 0, '77a70a77436103309ae2d78c44ac9048', 'judi.medhurst@yahoo.com', '来到星球也将近一周年了，趁着续费，发帖正式做个自我介绍。\n\n广州一本科班大二。大一绩点排前30%，专业保研率5%左右。保研无望，锚准就业方向，想做一名略懂前端的后端er。\n\n依稀记得，去年国庆假间进入了星球。从一个高考前从未接触过计算机的懵懂无知的小白，变成了如今每天在学习路上持续奔跑的萌新，这得益于互联网和现实中许许多多乐于助人、分享经验的大佬们。他们有鱼皮，有冯雨，有小林，有二哥...虽然是潜水一年，但也是我不断进步的一年。\n\n像大多数计算机大一er刚入学时一样，每天只能写着练习c语言语法的题目和黑漆漆的终端打招呼，没有一点视觉效果的呈现，甚感无趣甚至童年的我的对黑客的幻想彻底破灭。大一的9月机缘巧合下在b站关注了鱼皮也了解到了星球。刚进星球的前几个月，我都流连忘返于星球里对各个岗位丰富的技术栈的详细说明，原来庞大的互联网世界的背后同样精彩，这重新打起了我对计算机世界的兴趣。\n\n整个大一上，我还是过得很轻松，刚从高考的牢笼中挣脱出来的鸟儿比起看似遥远的就业更注重眼前的自由。所以除了看看星球了解下各种新鲜好玩的知识外，也只是跟着学校oj刷点题目。懵懵懂懂就结束了大一上学期。\n\n真正开始确立目标始于寒假期间。', 0, '15957925168', '2022-10-10 05:43:26', '2024-01-15 18:43:04', 0, '2', 0, '[\"Java\", \"C++\", \"Python\"]');
INSERT INTO `user` VALUES (2, '植龙', 'pwd12345678', 'https://images.zsxq.com/FtdfUySrGbAJTiYDzHSnsEvNeqJt?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:Ktod8jPIZOdi6NmOmjh8p5jbB8Y=', 0, '77a70a77436103309ae2d78c44ac9048', 'lilia.beier@hotmail.com', '今天是加入星球的第一天，我是一个普通一本学校的大二学生，不过我是从机械转来的，在大一我学了好多编程语言，c，c#，java，html好多都有涉及，加上我学了这些后也还能保持机械专业第三的成绩，人就特别飘，但经过一个暑假的洗礼，原本学的基本都忘了。\n\n然后现在学的也很痛苦，基本无基础的大二课程和需补修的大一课程已经把我安排的满满当当的，毫无没有大一的那种信手拈来。现在就是特别焦虑，但加入新球后我看到了很多学习打卡的天外的优秀的人，我现在也就更加焦虑了。\n\n所以我现在该怎么恢复好原来的状态，我也想要走后端的路，在看了鱼总的视频后我也有了个渺茫的目标，就是在一年后的暑假有能力去鱼总的公司实习（如果还招人的话），所以我现在该怎么安排自己，每天学习多少的后端知识够呢？', 0, '15749587054', '2022-02-18 23:42:35', '2024-01-15 18:42:16', 0, '3', 0, '[\"Java\", \"IOS\", \"Android\", \"Emo\", \"Happy\", \"School\", \"Freshman\"]');
INSERT INTO `user` VALUES (3, '黄蜂', 'sFH', 'https://images.zsxq.com/FvHo0Dp-FoRHwNDMUjaILBueqGuC?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:LRzfsTaGyRpSVeKo-H-vXqLe2vc=', 0, '77a70a77436103309ae2d78c44ac9048', 'kendall.hudson@hotmail.com', '悄悄的做个自我介绍，hhh，21届普通本科，计科专业，从学校到工作已经3年多了。目前在一个创业公司做后端开发，实际上什么都在做，后端开发进入的公司，因为什么都懂一点，然后和实习的公司产品有点类似，所以现在成了公司的全栈开发。\n\n选择这个专业完全属于爱好，从高中时就励志想成为计算机大佬，那时的转态和心态只有考上一个好的大学，从本地最好的示范性高中，考了一个普普通通的本科，学了计算机专业，从以前的满怀壮志，到选在的迷茫阶段。现在想回忆回忆从学校出来后怎么走到的现在。\n\n追溯到3+1实习阶段，刚开始和室友决定去成都找实习，谁知第一次去成都，然后找的面试，全是培训，然后说，培训以后直接入职，还要叫我交钱，当时的我很迷茫，根本不懂多少东西，就知道在csdn上搜一点八股文，潦草看几眼就去面试了，然后不意外的是竹篮打水一场空，无功而返，然后就回学校看学校的校招，就从从学校面试进入了北京超图的一个子公司，在培训阶段，一天100的工资除开房租，几乎还要家里倒贴，那时学习了js，gis，map3d等等，从最开始的地图怎么制作的，到配图，到最短路径规划和专题图分析等等，看到觉得做的东西好炫酷，但是实习了3个月，因为某种原因（xxx', 0, '17803172612', '2022-02-18 11:35:55', '2024-01-15 18:42:16', 0, '4', 0, '[\"Java\", \"IOS\", \"Happy\", \"Freshman\"]');
INSERT INTO `user` VALUES (4, '鱼浆', 'N9ES', 'https://images.zsxq.com/FowmQEWw8yjc4vzt3nEt33pxbxdh?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:JDy6xndNK6MTiiJ_GWsCejF1_0I=', 0, '77a70a77436103309ae2d78c44ac9048', 'lucila.brekke@gmail.com', '星球的小伙伴们，大家好，我是夏天，目前是一名在校大三专升本学生，17年读大专，19年自学了Java(学到了SpringBoot)，20年因为种种原因，去了一个可以保护大家的地方呆了两年，去年九月份回来后，参加了专升本考试，考上了了，从今年的3月份开始，我开始重新学习Java了，很高兴能在鱼皮哥的这个平台和优秀的大家一起学习进步', 0, '17820010802', '2022-04-28 07:52:08', '2024-01-15 18:42:16', 0, '5', 0, NULL);
INSERT INTO `user` VALUES (5, '文金', 'qIC2q', 'https://images.zsxq.com/FgDgTf2uMlrDFUNCFPnjJHHL8Lp9?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:kcFnL-4UJSHopnUBnFblC7d6fgU=', 0, '77a70a77436103309ae2d78c44ac9048', 'migdalia.upton@yahoo.com', '求职   找伙伴  我是一名专升本的学生，今天大四开学，然后之前在学校都是大班授课，自己也比较贪玩。现在跟了鱼皮哥的那个学习路线[加油][加油]，然后韩顺平的基础看了一大半了，现在人非常迷茫，想先找一份实习工作，有大佬指教一下吗', 0, '15767735929', '2022-08-10 09:43:17', '2024-01-15 18:42:16', 0, '6', 0, NULL);
INSERT INTO `user` VALUES (6, '越前龙', 'Zrw9', 'https://images.zsxq.com/FulR1cBNqrhTapUm1jBPhjIvfx7O?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:6bSjSZgJPuQBQcTz2Vpsk84ESYU=', 0, '77a70a77436103309ae2d78c44ac9048', 'rochel.swaniawski@yahoo.com', '大佬们好，我是211计科大二学生，大一感觉没学到啥，知道一些东西，但也不深，想提升自己，摸不清方向，求大佬指指道路', 0, '17046424932', '2022-04-13 02:33:47', '2024-01-15 18:43:04', 0, '7', 0, NULL);
INSERT INTO `user` VALUES (7, '奇迹龙', 'zi', 'https://images.zsxq.com/FgbNNPp0Pys5hYwypx_VF2Sh0MQQ?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:Mh6ZMBl6PIzicp2Mavq4bBrgjNQ=', 0, '77a70a77436103309ae2d78c44ac9048', 'aide.purdy@hotmail.com', '大佬们好呀，我是一名今年刚刚国贸毕业出来的学生，现在从业物流行业已经工作了两个多月，发现自己还是对计算机行业更感兴趣。想以后转职到计算机行业，却没有方向，有的就那颗倔强的心却不知道从哪里入手，还希望大佬们能指条明路  提问', 0, '15964705389', '2022-09-08 13:29:50', '2024-01-15 18:43:04', 0, '8', 0, '[\"Android\", \"Emo\", \"School\", \"Freshman\"]');
INSERT INTO `user` VALUES (8, '蜡膜', 'XK1T', 'https://images.zsxq.com/FuWRD-sedqmmVS2wkt4-3PZNsX_F?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:DEs3-t936N7folv7gj-ucwT_ObE=', 0, '77a70a77436103309ae2d78c44ac9048', 'albertha.lueilwitz@yahoo.com', '大佬们   非常需要大佬们的一些指点   相当焦虑  我现在是研三的阶段了  本硕双211（末流211）  计算机科学与技术专业  之前一直都在进行科研团队的科研项目  没有怎么准备工作这方面   导致到秋招在眼前的时候非常焦虑  八月底差不多28号才开始看Java 最近才刚刷完黑马的Javase基础课程 刚开始刷MySQL   有很多东西大学学过  所以刷的比较快  但很久没有用过了  科研阶段一直用Python跑深度模型  秋招项目的话要看的东西太多了有点来不及准备了   所以现在的目标就是银行的软开或者科技岗   现在主要是想在简历的技能上丰富一些  比如技术栈和项目部分   打算写上两个项目参加银行的秋招   银行的面试大概是在11月左右   有没有什么方法或者项目推荐能让我在11月前完成核心工作   能够应对银行的秋招面试   计算机专业只能说有基础但都没有学的很精细  现在就是不知道要直接上手一些项目  还是继续刷基础了  我感觉刷基础的话简历都不知道要放那些项目先投网申   害怕找的项目到面试阶段没有完成   被问到就会尬住了   所以特别需要大佬们推荐两三个上手快一些  能稍微拿得出手的项目 ', 0, '15759095129', '2022-01-13 04:01:13', '2024-01-15 18:43:04', 0, '9', 0, NULL);
INSERT INTO `user` VALUES (9, '萨摩', '2qXwT', 'https://images.zsxq.com/FsBzwReevG1WyKW4JM4nqfyrEfIo?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:uYlHUP5BnHZV6HXe4AfBGJBa54M=', 0, '77a70a77436103309ae2d78c44ac9048', 'darrick.sanford@hotmail.com', '找伙伴\n我今年大三，还是个菜鸡，今天第一天加入星球看见很多大佬发的动态，我想知道那些跟着大佬从 0 到 1 做项目的群都是在哪里加的呀', 0, '17320056390', '2022-04-21 12:00:44', '2024-01-15 18:43:04', 0, '10', 0, '[\"Java\", \"Happy\", \"School\", \"Freshman\"]');
INSERT INTO `user` VALUES (10, '种公猪', 'BTlT2', 'https://images.zsxq.com/FhjZuUO8jCUFOwliEqoGxNxzZGzb?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:DyT24mwbuS-8hse_NZr1TsYRNhM=', 0, '77a70a77436103309ae2d78c44ac9048', 'twanda.kassulke@yahoo.com', '大家好，本人22岁，以前是在一家线下机构学习（学完之后，线下机构的培训方面就被该公司砍掉了，理由是招不到人），9.2加入的星球，现在是在某个线上教育机构打工，在这之前是主玩爬虫，21年做过一个小程序项目，最近给公司做数据系统的时候突然感觉py的局限性让我很难受，把公司的数据系统做完以后，突然对web有了浓厚的兴趣   未来的发展方向想以JAVA为主，爬虫为副，来这里主要是想让自己明确JAVA的学习路线，也希望能认识各位大佬们', 0, '15212365438', '2022-06-28 05:20:39', '2024-01-15 18:43:04', 0, '11', 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
