package com.gzh.service;

import com.gzh.model.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 用户注册请求体
 *
 * @author GZH
 * @since 2024/1/17
 */
@Slf4j
@SpringBootTest
class InsertUsersTest {

    @Resource
    private UserService userService;

    private ExecutorService executorService = new ThreadPoolExecutor(60, 1000, 10000, TimeUnit.MINUTES, new ArrayBlockingQueue<>(10000));

    /**
     * 批量插入用户
     */
    @Test
    void doInsertUsers() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int INSERT_NUM = 100000;
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < INSERT_NUM; i++) {
            User user = this.initUser();
            userList.add(user);
        }
        // 30s 10w条 1k一次
        // 28s 10w条 1w一次
        //userService.saveBatch(userList, 10000);
        stopWatch.stop();
        log.info("共耗时 => {}", stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发批量插入用户
     */
    @Test
    void doConcurrencyInsertUsers() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int batchSize = 2500;
        final int loop = 40;
        // 分十组
        int j = 0;
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        for (int i = 0; i < loop; i++) {
            List<User> userList = new ArrayList<>();
            while (true) {
                j++;
                User user = this.initUser();
                userList.add(user);
                if (j % batchSize == 0) {
                    break;
                }
            }
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                System.out.println("ThreadName => " + Thread.currentThread().getName());
                //userService.saveBatch(userList, batchSize);
            }, executorService);
            futureList.add(future);
        }
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();
        stopWatch.stop();
        log.info("共耗时 => {}", stopWatch.getTotalTimeMillis());
    }

    private User initUser() {
        User user = new User();
        user.setUsername("FakerHal");
        user.setUserAccount("FakerHal");
        user.setAvatarUrl("https://images.zsxq.com/FgDgTf2uMlrDFUNCFPnjJHHL8Lp9?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:kcFnL-4UJSHopnUBnFblC7d6fgU=");
        user.setGender(0);
        user.setUserPassword("12345678");
        user.setPhone("123");
        user.setEmail("123@123.com");
        user.setUserStatus(0);
        user.setUserRole(0);
        user.setTags("[]");
        user.setPlanetCode("11111");
        return user;
    }
}
