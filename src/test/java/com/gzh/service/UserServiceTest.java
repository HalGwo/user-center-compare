package com.gzh.service;

import com.gzh.model.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author GZH
 * @version 1.0
 * @since 2022/9/7 21:21
 */
@SpringBootTest
class UserServiceTest {


    @Resource
    private UserService userService;

    @Test
    void testAddUser() {
        User user = new User();
        user.setUsername("GZH");
        user.setUserAccount("GZH");
        user.setAvatarUrl("https://plugins.jetbrains.com/files/7973/208337/icon/pluginIcon.svg");
        user.setGender(0);
        user.setUserPassword("XXX");
        user.setPhone("123");
        user.setEmail("456");
        boolean result = userService.save(user);
        System.out.println(user.getId());
        Assertions.assertTrue(result);
    }

    //按序号批量删除-更改for的大小
    @Test
    void testDeleteUser() {
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < 28; i++)
            ids.add(i);

        boolean result = userService.removeBatchByIds(ids);
        Assertions.assertTrue(result);
    }

    @Test
    void s() {
        boolean result = userService.removeById(29);
        Assertions.assertTrue(result);
    }

    @Test
    void userRegister() {
        String userAccount = "GZH";
        String userPassword = "";
        String checkPassword = "XXX";
        String planetCode = "1";
        Long result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "GZ";
        result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "GZH";
        userPassword = "XXX";
        result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "GZ H";
        userPassword = "12345678";
        result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
        checkPassword = "123456789";
        result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "ZGZH";
        checkPassword = "123";
        result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "GZH";
        result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
    }

    @Test
    void register() {
        String userAccount = "GZHm";
        String userPassword = "12345678";
        String checkPassword = "12345678";
        String planetCode = "1";
        Long result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        Assertions.assertEquals(-1, result);
    }

    @Test
    void testSearchUsersByTags() {
        List<String> tagList = Arrays.asList("Java", "Python", "难");
        List<User> userList = userService.searchUsersByTags(tagList);
        Assertions.assertNotNull(userList);
    }

}