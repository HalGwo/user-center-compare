package gzk;

import lombok.Data;

/**
 * @author GZH
 * @version 1.0
 * @since 2022/12/12 13:53
 */
@Data
public class score {
    int no;
    String sName;
    int sNum;
    String cName;
    double score;

   public String info() {
        return no + sName + sNum + cName + score;
   }
}
