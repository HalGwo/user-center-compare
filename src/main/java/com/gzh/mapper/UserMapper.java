package com.gzh.mapper;

import com.gzh.model.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 针对表【user(用户)】的数据库操作Mapper
 *
 * @author BatComputer
 * @since 2022-09-07 19:01:01
 */
public interface UserMapper extends BaseMapper<User> {

}




