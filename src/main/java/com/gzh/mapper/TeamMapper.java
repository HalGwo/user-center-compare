package com.gzh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzh.model.domain.Team;

/**
 * 针对表【team(队伍)】的数据库操作Mapper
 *
 * @author Hal
 * @since 2024-01-24 14:43:11
 */
public interface TeamMapper extends BaseMapper<Team> {

}




