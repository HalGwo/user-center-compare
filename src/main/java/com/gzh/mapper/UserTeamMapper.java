package com.gzh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzh.model.domain.UserTeam;

/**
 * 针对表【user_team(用户队伍关系)】的数据库操作Mapper
 *
 * @author Hal
 * @since 2024-01-24 14:44:32
 */
public interface UserTeamMapper extends BaseMapper<UserTeam> {

}




