package com.gzh.exception;

import com.gzh.common.ErrorCode;
import lombok.Getter;

/**
 * 自定义异常类
 *
 * @author GZH
 * @since 2023/7/31 20:17
 */
@Getter
public class BusinessException extends RuntimeException{

    private static final long serialVersionUID = 3259795799549227448L;

    private final int code;

    private final String description;

    public BusinessException(int code, String message, String description){
        super(message);
        this.code = code;
        this.description = description;
    }

    public BusinessException(ErrorCode errorCode){
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = errorCode.getMessage();
    }

    public BusinessException(ErrorCode errorCode, String description){
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = description;
    }
}
