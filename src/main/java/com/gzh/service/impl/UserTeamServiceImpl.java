package com.gzh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzh.mapper.UserTeamMapper;
import com.gzh.model.domain.UserTeam;
import com.gzh.model.vo.TeamUserVO;
import com.gzh.service.UserTeamService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 针对表【user_team(用户队伍关系)】的数据库操作Service实现
 *
 * @author Hal
 * @since 2024-01-24 14:44:32
 */
@Service
public class UserTeamServiceImpl extends ServiceImpl<UserTeamMapper, UserTeam> implements UserTeamService {

    @Override
    public long countTeamUser(Long teamId) {
        LambdaQueryWrapper<UserTeam> lqw = new LambdaQueryWrapper<>();
        lqw.eq(teamId != null && teamId > 0, UserTeam::getTeamId, teamId);
        return this.count(lqw);
    }

    @Override
    public long countTeamUser(Long teamId, Long userId) {
        LambdaQueryWrapper<UserTeam> lqw = new LambdaQueryWrapper<>();
        // 根据队伍id查询
        lqw.eq(teamId != null && teamId > 0, UserTeam::getTeamId, teamId);
        // 根据用户id查询
        lqw.eq(userId != null && userId > 0, UserTeam::getUserId, userId);
        return this.count(lqw);
    }

    @Override
    public List<TeamUserVO> setHasJoinTeam(List<TeamUserVO> teamList, Long userId) {
        // 获取所有队伍id
        final List<Long> teamIdList = teamList.stream().map(TeamUserVO::getId).collect(Collectors.toList());
        LambdaQueryWrapper<UserTeam> lqw = new LambdaQueryWrapper<>();
        lqw.eq(UserTeam::getUserId, userId);
        lqw.in(UserTeam::getTeamId, teamIdList);
        List<UserTeam> userTeamList = this.list(lqw);
        // 已加入的队伍 id 集合
        Set<Long> hasJoinTeamIdSet = userTeamList.stream().map(UserTeam::getTeamId).collect(Collectors.toSet());
        teamList.forEach(team -> {
            // 遍历所有队伍id 看是否在当前用户 已加入的队伍列表里
            boolean hasJoin = hasJoinTeamIdSet.contains(team.getId());
            team.setHasJoin(hasJoin);
        });
        return teamList;
    }

    @Override
    public List<TeamUserVO> setJoinedTeamNum(List<TeamUserVO> teamList) {
        // 获取所有队伍id 查出队伍用户列表
        final List<Long> teamIdList = teamList.stream().map(TeamUserVO::getId).collect(Collectors.toList());
        LambdaQueryWrapper<UserTeam> lqw = new LambdaQueryWrapper<>();
        lqw.in(UserTeam::getTeamId, teamIdList);
        List<UserTeam> userTeamList = this.list(lqw);
        // 队伍 id => 加入队伍的用户列表 (根据队伍id进行分组)
        Map<Long, List<UserTeam>> teamIdUserTeamList = userTeamList.stream().collect(Collectors.groupingBy(UserTeam::getTeamId));

        // 在每个队伍中提取出该队伍所有用户id 存入map<teamId, userIds>
        Map<Long, List<Long>> teamUserIdsMap = new HashMap<>();
        for (Map.Entry<Long, List<UserTeam>> entry : teamIdUserTeamList.entrySet()) {
            List<Long> userIdList = entry.getValue().stream().map(UserTeam::getUserId).collect(Collectors.toList());
            teamUserIdsMap.put(entry.getKey(), userIdList);
        }

        teamList.forEach(team -> {
            // 将每个队伍的人数放入字段中一起返回                 做一个非空校验
            List<UserTeam> teamUserNum = teamIdUserTeamList.getOrDefault(team.getId(), Collections.emptyList());
            team.setHasJoinNum(teamUserNum.size());
            // 队伍下所有用户id存入该队伍list
            team.setTeamUserIds(teamUserIdsMap.get(team.getId()));
        });
        return teamList;
    }

    @Override
    public LambdaQueryWrapper<UserTeam> sqlCondition(UserTeam userTeam) {
        LambdaQueryWrapper<UserTeam> lqw = new LambdaQueryWrapper<>();
        if (userTeam == null) {
            return lqw;
        }
        // 根据队伍id查询
        Long teamId = userTeam.getTeamId();
        lqw.eq(teamId != null && teamId > 0, UserTeam::getTeamId, teamId);

        // 根据用户id查询
        Long userId = userTeam.getUserId();
        lqw.eq(userId != null && userId > 0, UserTeam::getUserId, userId);
        return lqw;
    }
}




