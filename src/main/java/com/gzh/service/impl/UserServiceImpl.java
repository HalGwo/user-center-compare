package com.gzh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gzh.common.ErrorCode;
import com.gzh.exception.BusinessException;
import com.gzh.mapper.UserMapper;
import com.gzh.model.domain.User;
import com.gzh.service.UserService;
import com.gzh.utils.AlgorithmUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.gzh.constant.UserConstant.ADMIN_ROLE;
import static com.gzh.constant.UserConstant.USER_LOGIN_STATE;

/**
 * @author BatComputer
 * 针对表【user(用户)】的数据库操作Service实现
 * @since 2022-09-07 19:01:01
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    /**
     * 特殊字符正则匹配
     */
    @SuppressWarnings("all")
    private static final String VALID_PATTERN = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";

    /**
     * 盐值 混淆密码
     */
    private static final String SALT = "GZH";

    @Override
    public long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode) {
        // 1.校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            // TODO 修改为自定义异常
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户账号过短");
        }
        if (userPassword.length() < 8 || checkPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户密码过短");
        }
        if (planetCode.length() > 5) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "星球编号过长");
        }
        //账户不能包含特殊字符
        Matcher matcher = Pattern.compile(VALID_PATTERN).matcher(userAccount);
        if (matcher.find()) {
            return -1;
        }
        // 密码和校验密码相同
        if (!userPassword.equals(checkPassword)) {
            return -1;
        }
        // 账户不能重复
        QueryWrapper<User> qw = new QueryWrapper<>();
        long count = this.count(qw.eq("userAccount", userAccount));
        if (count > 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号重复");
        }

        // 星球编号不能重复
        qw = new QueryWrapper<>();
        qw.eq("planetCode", planetCode);
        count = baseMapper.selectCount(qw);
        if (count > 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "编号重复");
        }

        // 2.加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
        // 3.插入数据
        User user = new User();
        user.setUserAccount(userAccount);
        user.setUserPassword(encryptPassword);
        user.setPlanetCode(planetCode);
        user.setAvatarUrl("https://plugins.jetbrains.com/files/7973/208337/icon/pluginIcon.svg");
        boolean saveResult = this.save(user);
        if (!saveResult) {
            return -1;
        }
        return user.getId();
    }

    @Override
    public User userLogin(String userAccount, String userPassword, HttpServletRequest request) {
        // 1.校验
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号或密码为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号错误");
        }
        if (userPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码错误");
        }
        //账户不能包含特殊字符
        Matcher matcher = Pattern.compile(VALID_PATTERN).matcher(userAccount);
        if (matcher.find()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号不能有特殊字符");
        }
        // 2.加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());

        // 查询用户是否存在 pwd 12345678
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("userAccount", userAccount);
        qw.eq("userPassword", encryptPassword);
        User user = baseMapper.selectOne(qw);
        // 用户不存在
        if (user == null) {
            log.info("user login failed,userAccount cannot match userPassword");
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户名或密码错误");
        }

        //3.用户脱敏 去除敏感信息
        User safetyUser = getSafetyUser(user);

        // 4.记录用户的登录态
        request.getSession().setAttribute(USER_LOGIN_STATE, safetyUser);

        return safetyUser;
    }

    @Override
    public User getSafetyUser(User originUser) {
        if (originUser == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User safetyUser = new User();
        safetyUser.setId(originUser.getId());
        safetyUser.setUsername(originUser.getUsername());
        safetyUser.setUserAccount(originUser.getUserAccount());
        safetyUser.setAvatarUrl(originUser.getAvatarUrl());
        safetyUser.setGender(originUser.getGender());
        safetyUser.setPhone(originUser.getPhone());
        safetyUser.setEmail(originUser.getEmail());
        safetyUser.setProfile(originUser.getProfile());
        safetyUser.setUserRole(originUser.getUserRole());
        safetyUser.setUserStatus(originUser.getUserStatus());
        safetyUser.setPlanetCode(originUser.getPlanetCode());
        safetyUser.setCreateTime(originUser.getCreateTime());
        safetyUser.setTags(originUser.getTags());
        return safetyUser;
    }

    @Override
    public int userLogout(HttpServletRequest request) {
        // 移除登录态
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }

    @Override
    public List<User> searchUsersByTags(List<String> tagNameList) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        Gson gson = new Gson();
        List<User> userList = baseMapper.selectList(null);
        return userList.stream().filter(user -> {
            String tagsNames = user.getTags();
            if (StringUtils.isBlank(tagsNames)) {
                return false;
            }
            Set<String> tagList = gson.fromJson(tagsNames, new TypeToken<Set<String>>() {
            }.getType());
            tagList = Optional.ofNullable(tagList).orElse(Collections.emptySet());
            for (String tagName : tagNameList) {
                if (tagList.contains(tagName)) {
                    return true;
                }
            }
            return false;
        }).map(this::getSafetyUser).collect(Collectors.toList());
    }

    @Override
    public int updateUser(User user, User loginUser) {
        Long userId = user.getId();
        if (userId < 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        // 如果是管理员 允许更新任意用户 否则只允许更新当前用户信息
        if (!isAdmin(loginUser) && !userId.equals(loginUser.getId())) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        User oldUser = baseMapper.selectById(userId);
        if (oldUser == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        return baseMapper.updateById(user);
    }

    @Override
    public User getLoginUser(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        if (userObj == null) {
            throw new BusinessException(ErrorCode.N0T_L0GIN);
        }
        return (User) userObj;
    }

    @Override
    public boolean isAdmin(HttpServletRequest request) {
        // 仅管理员可操作
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User) userObj;
        return this.isAdmin(user);
    }

    @Override
    public boolean isAdmin(User loginUser) {
        // 仅管理员可操作
        return loginUser != null && loginUser.getUserRole() == ADMIN_ROLE;
    }

    /**
     * 根据标签搜索用户 (SQL 查询版)
     *
     * @param tagNameList 用户要拥有的标签
     * @return userList
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings({"all"})
    private List<User> searchUsersByTagsBySql(List<String> tagNameList) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // 拼接 and 查询
        // like '%Java%' and like '%Python%'
        for (String tagName : tagNameList) {
            queryWrapper = queryWrapper.like("tags", tagName);
        }
        List<User> userList = baseMapper.selectList(queryWrapper);

        return userList.stream().map(this::getSafetyUser).collect(Collectors.toList());
    }

    @Override
    public List<User> matchUsers(long num, User loginUser) {
        // 查出所有用户的id和tags
        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<>();
        lqw.select(User::getId, User::getTags);
        List<User> userIdTagsList = this.list(lqw);

        // new一个排序list 根据分数排序
        List<Pair<User, Integer>> pairList = new ArrayList<>();
        Gson gson = new Gson();
        // 标签列表tags字符串转list格式
        List<String> currentTagsList = gson.fromJson(loginUser.getTags(), new TypeToken<List<String>>() {}.getType());
        // 遍历所有用户标签
        for (User user : userIdTagsList) {
            // 排除空标签与自己
            if (StringUtils.isBlank(user.getTags()) || loginUser.getId().equals(user.getId())) {
                continue;
            }
            List<String> otherTagsList = gson.fromJson(user.getTags(), new TypeToken<List<String>>() {}.getType());
            // 计算当前用户标签与 查数据库中用户的标签 的相似度 (编辑距离算法)
            // TODO 如果匹配数量不足num 会出现毫无相关的人 分数为otherTagsList().size()
            int scoreSimilar = AlgorithmUtils.minDistance(currentTagsList, otherTagsList);
            // 在用Pair对列表排序
            Pair<User, Integer> userSimilar = new Pair<>(user, scoreSimilar);
            pairList.add(userSimilar);
        }

        // 最后进行排序 并且保留前num个用户 (获取最匹配的前num个用户)
        List<Pair<User, Integer>> similarUserList = pairList.stream()
                .sorted(Comparator.comparingInt(Pair::getValue))
                .limit(num)
                .collect(Collectors.toList());
        // 获取最佳匹配用户的id
        List<Long> userIdList = similarUserList.stream().map(pair -> pair.getKey().getId()).collect(Collectors.toList());
        // 在根据id查询出相应用户信息
        lqw.clear();
        lqw.in(User::getId, userIdList);
        List<User> resultUserList = this.list(lqw);

        // 将用户按照id进行分组 顺便将用户进行脱敏
        Map<Long, List<User>> userIdMap = resultUserList.stream()
                .map(this::getSafetyUser)
                .collect(Collectors.groupingBy(User::getId));
        // 由于用in查询sql之后排序又乱了 所以要重新用之前得出的id顺序来排序
        resultUserList = new ArrayList<>();
        for (Pair<User, Integer> pairUser : similarUserList) {
            User user = userIdMap.get(pairUser.getKey().getId()).get(0);
            resultUserList.add(user);
        }
        return resultUserList;
    }
}




