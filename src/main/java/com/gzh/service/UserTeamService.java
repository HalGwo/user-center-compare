package com.gzh.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzh.model.domain.UserTeam;
import com.gzh.model.vo.TeamUserVO;

import java.util.List;

/**
* @author Hal
* 针对表【user_team(用户队伍关系)】的数据库操作Service
* @since 2024-01-24 14:44:32
*/
public interface UserTeamService extends IService<UserTeam> {

    /**
     * 获取某队伍当前人数
     *
     * @param teamId 队伍id
     * @return 当前队伍人数
     */
    long countTeamUser(Long teamId);

    /**
     * 获取某队伍当前人数
     *
     * @param teamId 队伍id
     * @return 当前队伍人数
     */
    long countTeamUser(Long teamId, Long userId);


    /**
     * 获取某队伍当前人数
     *
     * @param teamList 队伍列表
     * @param userId 用户id
     * @return
     */
    List<TeamUserVO> setHasJoinTeam(List<TeamUserVO> teamList, Long userId);


    /**
     * 获取某队伍当前人数
     *
     * @param teamList 队伍列表
     * @return list
     */
    List<TeamUserVO> setJoinedTeamNum(List<TeamUserVO> teamList);

    /**
     * 生成MP查询条件
     *
     * @param userTeam 查询条件
     * @return lambda条件
     */
    LambdaQueryWrapper<UserTeam> sqlCondition(UserTeam userTeam);
}
