package com.gzh.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzh.model.domain.Team;
import com.gzh.model.domain.User;
import com.gzh.model.dto.TeamQuery;
import com.gzh.model.request.TeamJoinRequest;
import com.gzh.model.request.TeamQuitRequest;
import com.gzh.model.request.TeamUpdateRequest;
import com.gzh.model.vo.TeamUserVO;

import java.util.List;

/**
 * @author Hal
 * 针对表【team(队伍)】的数据库操作Service
 * @since 2024-01-24 14:43:11
 */
public interface TeamService extends IService<Team> {

    /**
     * 检测队伍创建信息是否正确
     *
     * @param team      创建队伍的信息
     * @param loginUser 创建人
     */
    void checkTeam(Team team, User loginUser);

    /**
     * 创建队伍
     *
     * @param team      创建队伍的信息
     * @param loginUser 创建人
     * @return count
     */
    long createTeam(Team team, User loginUser);

    /**
     * 搜索队伍
     *
     * @param teamQuery 查询参数
     * @param isAdmin   管理员校验
     * @return list
     */
    List<TeamUserVO> listTeams(TeamQuery teamQuery, boolean isAdmin);

    /**
     * 更新队伍信息
     *
     * @param teamUpdateRequest 更新参数
     * @param loginUser         修改者
     * @return 布尔
     */
    boolean updateTeam(TeamUpdateRequest teamUpdateRequest, User loginUser);

    /**
     * 用户加入队伍 并校验
     *
     * @param teamJoinRequest 参数
     * @param loginUser       用户
     * @return 布尔
     */
    boolean joinTeam(TeamJoinRequest teamJoinRequest, User loginUser);

    /**
     * 退出队伍
     *
     * @param teamQuitRequest 参数
     * @param loginUser       退出的用户
     * @return 布尔
     */
    boolean quitTeam(TeamQuitRequest teamQuitRequest, User loginUser);

    /**
     * 解散队伍
     *
     * @param id        队伍id
     * @param loginUser 队长id
     * @return 布尔
     */
    boolean deleteTeam(long id, User loginUser);

    /**
     * 根据 id 获取队伍信息
     *
     * @param teamId 队伍id
     * @return 队伍
     */
    Team getTeamById(Long teamId);

    /**
     * 生成MP查询条件
     *
     * @param teamQuery 查询条件
     * @return lambda条件
     */
    LambdaQueryWrapper<Team> sqlCondition(TeamQuery teamQuery);

    /**
     * 生成MP查询条件 (多一个条件 并且需要校验身份 根据状态来查询)
     *
     * @param teamQuery 查询条件
     * @param isAdmin   是否管理员
     * @return lambda条件
     */
    LambdaQueryWrapper<Team> sqlCondition(TeamQuery teamQuery, boolean isAdmin);
}
