package com.gzh.constant;

/**
 * @author GZH
 * @version 1.0
 * @since 2022/11/13 10:40
 */

public interface UserConstant {

    /**
     * 用户登录态
     */
    public static final String USER_LOGIN_STATE = "userLoginState";

    // ------- 权限 -------

    /**
     * 默认权限
     */
    int DEFAULT_ROLE = 0;

    /**
     * 管理员权限
     */
    int ADMIN_ROLE = 1;

}
