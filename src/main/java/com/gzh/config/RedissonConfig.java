package com.gzh.config;

import lombok.Data;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 用户注册请求体
 *
 * @author GZH
 * @since 2024/1/23
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
public class RedissonConfig {

    private String host;
    private String port;

    @Bean
    public RedissonClient redissonClient() {
        // 1.配置redis
        Config config = new Config();
        String redisAddr = String.format("redis://%s:%s", host, port);
        config.useSingleServer().setAddress(redisAddr).setDatabase(3);
        // 2. 新建实例并返回
        return Redisson.create(config);
    }
}
