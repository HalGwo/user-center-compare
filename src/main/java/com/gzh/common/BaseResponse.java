package com.gzh.common;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * 通用返回结果集
 *
 * @author GZH
 * @version 1.0
 * @since 2023/7/27 20:01
 */
@Data
public class BaseResponse<T> implements Serializable {
    private static final long serialVersionUID = -8773178804980088273L;

    private int code;

    @SuppressWarnings("squid:S1948")
    private T data;

    private String message;

    private String description;

    public BaseResponse(int code, T data, String message, String description) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.description = description;
    }

    public BaseResponse(int code, T data, String message) {
        this(code, data, message, EMPTY);
    }

    public BaseResponse(int code, T data) {
        this(code, data, EMPTY, EMPTY);
    }

    public BaseResponse(ErrorCode errorCode) {
        this(errorCode.getCode(), null, errorCode.getMessage(), errorCode.getDescription());
    }
}
