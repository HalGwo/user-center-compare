package com.gzh.common;

import lombok.AllArgsConstructor;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * @author GZH
 * @version 1.0
 * @since 2023/7/27 20:46
 */
@AllArgsConstructor
public enum ErrorCode {
    SUCCESS(0, "ok", EMPTY),

    PARAMS_ERROR(40000, "请求参数错误", EMPTY),

    NULL_ERROR(40001, "请求数据为空", EMPTY),

    N0T_L0GIN(40100, "未登录", EMPTY),

    NO_AUTH(40101, "无权限", EMPTY),

    FORBIDDEN(40301, "禁止操作", EMPTY),

    SYSTEM_ERROR(50000, "系统内部异常", EMPTY);

    /**
     * 状态码
     */
    private final int code;

    /**
     * 状态码信息
     */
    private final String message;

    /**
     * 状态码描述 (详情)
     */
    private final String description;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
