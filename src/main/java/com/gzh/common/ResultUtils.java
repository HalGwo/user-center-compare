package com.gzh.common;

/**
 * 返回工具栏
 *
 * @author GZH
 * @version 1.0
 * @since 2023/7/27 20:30
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class ResultUtils {
    private ResultUtils (){}

    /**
     * 成功
     *
     * @param data 返回数据
     * @param <T>  返回类型
     * @return baseResponse
     */
    public static <T> BaseResponse<T> success(T data) {
        return new BaseResponse<>(0, data, "ok");
    }

    /**
     * 失败
     *
     * @param errorCode 错误信息
     * @return baseResponse
     */
    public static BaseResponse error(ErrorCode errorCode) {
        return new BaseResponse<>(errorCode);
    }

    /**
     * 失败
     */
    public static BaseResponse error(int code, String message, String description) {
        return new BaseResponse(code, null, message, description);
    }

    /**
     * 失败
     */
    public static BaseResponse error(ErrorCode errorCode, String message, String description) {
        return new BaseResponse(errorCode.getCode(), null, message, description);
    }

    /**
     * 失败
     */
    public static BaseResponse error(ErrorCode errorCode, String description) {
        return new BaseResponse<>(errorCode.getCode(), errorCode.getMessage(), description);
    }
}
