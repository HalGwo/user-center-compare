package com.gzh.once;

import com.alibaba.excel.EasyExcel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 导入星球用户到数据库
 *
 * @author GZH
 * @since 2024/1/12
 */
@Slf4j
public class ImportPlanetUser {

    /**
     * 读取数据
     */
    @SuppressWarnings("all")
    public static void main(String[] args) {
        //final String fileName = "D:\\GZH\\yupi_pro\\compare\\user-center-spring\\src\\main\\resources\\TestExcel.xlsx";
        final String fileName = "D:\\GZH\\yupi_pro\\compare\\user-center-spring\\src\\main\\resources\\prodExcel.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 同步读取会自动finish
        List<PlanetUserTableInfo> list = EasyExcel.read(fileName).head(PlanetUserTableInfo.class).sheet().doReadSync();
        for (PlanetUserTableInfo data : list) {
            log.info("data => {}", data);
        }
        Map<String, List<PlanetUserTableInfo>> usernameMap = list.stream()
                .filter(item -> StringUtils.isNotBlank(item.getUsername()))
                .collect(Collectors.groupingBy(PlanetUserTableInfo::getUsername));
        log.info("总数 => {}", list.size());
        log.info("不重复 => {}", usernameMap.size());
        for (Map.Entry<String, List<PlanetUserTableInfo>> entry : usernameMap.entrySet()) {
            if (entry.getValue().size() > 1) {
                log.info(entry.getKey());
            }
        }
    }
}
