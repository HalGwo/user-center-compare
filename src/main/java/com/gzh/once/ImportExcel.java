package com.gzh.once;

import com.alibaba.excel.EasyExcel;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 从Excel中获取数据
 *
 * @author GZH
 * @since 2024/1/12
 */
@Slf4j
public class ImportExcel {

    /**
     * 读取数据
     */
    public static void main(String[] args) {
        final String fileName = "D:\\GZH\\yupi_pro\\compare\\user-center-spring\\src\\main\\resources\\TestExcel.xlsx";
        readByListener(fileName);
        synchronousRead(fileName);
    }

    public static void readByListener(String fileName) {
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        // 这里每次会读取100条数据 然后返回过来 直接调用使用数据就行
        EasyExcel.read(fileName, PlanetUserTableInfo.class, new TableListener()).sheet().doRead();
    }

    public static void synchronousRead(String fileName) {
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 同步读取会自动finish
        List<PlanetUserTableInfo> list = EasyExcel.read(fileName).head(PlanetUserTableInfo.class).sheet().doReadSync();
        for (PlanetUserTableInfo data : list) {
            log.info("data => {}", data);
        }
    }
}
