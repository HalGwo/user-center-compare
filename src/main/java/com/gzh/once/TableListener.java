package com.gzh.once;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import lombok.extern.slf4j.Slf4j;

/**
 * 有个很重要的点 PlanetUserTableInfoListener
 * 不能被spring管理 要每次读取excel都要new
 * 然后里面用到spring可以构造方法传进去
 *
 * @author Hal
 */
@Slf4j
public class TableListener implements ReadListener<PlanetUserTableInfo> {

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     */
    @Override
    public void invoke(PlanetUserTableInfo data, AnalysisContext context) {
        log.info("data => {}", data);
    }

    /**
     * 所有数据解析完成了 都会来调用
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("已解析完成");
    }
}