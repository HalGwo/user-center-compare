package com.gzh.once;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * @author Hal
 */
@Slf4j
public class CatchPlanetInfoUtil {
    public static void main(String[] args) throws Exception {
        OkHttpClient client = new OkHttpClient();
        
        // Curl命令中的URL
        String url = "https://api.zsxq.com/v2/hashtags/48844541281228/topics?count=20&end_time=2024-01-03T21%3A26%3A02.267%2B0800";
        
        Request request = new Request.Builder()
                .url(url)
                .build();
                
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }

            ResponseBody body = response.body();
            MediaType contentType = body != null ? body.contentType() : null;
            log.info("Content-Type: " + contentType);
            
            // 输出服务器返回的数据
            log.info(body.string());
        } catch (Exception e) {
            log.error("exception => {}", e.getMessage());
        }
    }
}