package com.gzh.once;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 星球表格用户信息
 *
 * @author GZH
 * @since 2024/1/12
 */
@Data
@EqualsAndHashCode
public class PlanetUserTableInfo {
    /**
     * id
     */
    @ExcelProperty("成员编号")
    private String planetCode;

    /**
     * 用户昵称
     */
    @ExcelProperty("role")
    private String username;

    /**
     * 内容
     */
    @ExcelProperty("content")
    private String content;

    /**
     * 头像
     */
    @ExcelProperty("avatar")
    private String avatar;

    /**
     * 用户昵称
     */
    @ExcelProperty("emoji_local src")
    private String emoji;
}