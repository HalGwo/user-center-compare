package com.gzh.once;

import com.gzh.mapper.UserMapper;
import com.gzh.model.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;

/**
 * 用户注册请求体
 *
 * @author GZH
 * @since 2024/1/17
 */
@Slf4j
@Component
public class InsertTestUser {
    @Resource
    private UserMapper userMapper;

    /**
     * 批量插入用户
     */
    private void doInsertUsers() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int INSERT_NUM = 10000000;
        for (int i = 0; i < INSERT_NUM; i++) {
            User user = new User();
            user.setUsername("FakerHal");
            user.setUserAccount("FakerHal");
            user.setAvatarUrl("https://images.zsxq.com/FgDgTf2uMlrDFUNCFPnjJHHL8Lp9?e=1709222399&token=kIxbL07-8jAj8w1n4s9zv64FuZZNEATmlU_Vm6zD:kcFnL-4UJSHopnUBnFblC7d6fgU=");
            user.setGender(0);
            user.setUserPassword("12345678");
            user.setPhone("123");
            user.setEmail("123@123.com");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setTags("[]");
            user.setPlanetCode("11111");
            userMapper.insert(user);
        }
        stopWatch.stop();
        log.info("共耗时 => {}", stopWatch.getTotalTimeMillis());
    }
}
