package com.gzh.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzh.common.BaseResponse;
import com.gzh.common.ErrorCode;
import com.gzh.common.ResultUtils;
import com.gzh.exception.BusinessException;
import com.gzh.model.domain.User;
import com.gzh.model.request.UserLoginRequest;
import com.gzh.model.request.UserRegisterRequest;
import com.gzh.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.gzh.constant.UserConstant.USER_LOGIN_STATE;



/**
 * 用户接口
 *
 * @author GZH
 * @version 1.0
 * @since 2022/11/12 11:41
 */
//@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/user")
@CrossOrigin(origins = {"http://localhost:8080","http://localhost:80","http://localhost:8081","http://localhost:5173","http://localhost:3000"}, allowCredentials="true")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @PostMapping("/register")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest userRegisterRequest) {
        if (userRegisterRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        String planetCode = userRegisterRequest.getPlanetCode();
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword, planetCode)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        long result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        return ResultUtils.success(result);
    }

    /**
     * test
     * {
     *   "userAccount": "GZHm",
     *   "userPassword": 12345678
     * }
     */
    @PostMapping("/login")
    public BaseResponse<User> userLogin(@RequestBody UserLoginRequest userLoginRequest, HttpServletRequest request) {
        if (userLoginRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = userService.userLogin(userAccount, userPassword, request);
        return ResultUtils.success(user);
    }

    @PostMapping("/logout")
    public BaseResponse<Integer> userLogout(HttpServletRequest request) {
        if (request == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        int result = userService.userLogout(request);
        return ResultUtils.success(result);
    }

    @GetMapping("/current")
    public BaseResponse<User> getCurrentUser(HttpServletRequest request) {
        User currentUser = (User) request.getSession().getAttribute(USER_LOGIN_STATE);
        if (currentUser == null){
            throw new BusinessException(ErrorCode.N0T_L0GIN);
        }
        // TODO 校验用户是否合法
        Long userId = currentUser.getId();
        User user = userService.getById(userId);
        User safetyUser = userService.getSafetyUser(user);
        this.initGender(safetyUser);
        return ResultUtils.success(safetyUser);
    }

    @GetMapping("/search")
    public BaseResponse<List<User>> searchUsers(String username, HttpServletRequest request) {
        if (!userService.isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTH, "缺少管理员权限");
        }
        QueryWrapper<User> qw = new QueryWrapper<>();
        if (StringUtils.isNoneBlank(username)) {
            qw.like("username", username);
        }
        List<User> userList = userService.list(qw);
        List<User> result = userList.stream()
                .map(user -> userService.getSafetyUser(user))
                .collect(Collectors.toList());
        return ResultUtils.success(result);
    }

    @GetMapping("/recommend")
    @SuppressWarnings("unchecked")
    public BaseResponse<Page<User>> searchRecommendUsers(long pageSize, long pageNum, HttpServletRequest request) {
        // todo 推荐多个，未实现
        User loginUser = userService.getLoginUser(request);
        String redisKey = String.format("teamup:user:recommend:%s", loginUser.getId());
        ValueOperations<String, Object> redis = redisTemplate.opsForValue();
        // 有缓存则直接返回
        Page<User> userPage = (Page<User>) redis.get(redisKey);
        //if (userPage != null) {
        //    return ResultUtils.success(userPage);
        //}
        // 否则直接查数据库
        QueryWrapper<User> qw = new QueryWrapper<>();
        userPage = userService.page(new Page<>(pageNum, pageSize), qw);
        // 还要将数据存入缓存 方便下次直接获取
        try {
            redis.set(redisKey, userPage, 30000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("redis set key error", e);
        }
        log.error("UserTag ====================> {}", userPage.getRecords().get(0).getTags());
        return ResultUtils.success(userPage);
    }
    @GetMapping("/search/tags")
    public BaseResponse<List<User>> searchUsersByTags(@RequestParam(required = false) List<String> tagNameList) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请求参数为空");
        }
        List<User> userList = userService.searchUsersByTags(tagNameList);
        return ResultUtils.success(userList);
    }

    @PostMapping("/update")
    public BaseResponse<Integer> updateUser(@RequestBody User user, HttpServletRequest request) {
        // 参数非空校验
        if (user == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        int update = userService.updateUser(user, loginUser);
        return ResultUtils.success(update);
    }

    @DeleteMapping("/delete")
    public BaseResponse<Boolean> deleteUsers(long id, HttpServletRequest request) {
        if (!userService.isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean result = userService.removeById(id);
        return ResultUtils.success(result);
    }

    /**
     * 获取最匹配的用户
     */
    @GetMapping("/match")
    public BaseResponse<List<User>> matchUsers(long num, HttpServletRequest request) {
        if (num <= 0 || num > 20) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = userService.getLoginUser(request);
        return ResultUtils.success(userService.matchUsers(num, user));
    }

    private void initGender(User user) {
        if (user == null || user.getGender() == null) {
            return;
        }
        user.setGenderStr("女");
        if (user.getGender().equals(0)) {
            user.setGenderStr("男");
        }
    }
}
