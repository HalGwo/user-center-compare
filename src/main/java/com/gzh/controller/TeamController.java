package com.gzh.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzh.common.BaseResponse;
import com.gzh.common.DeleteRequest;
import com.gzh.common.ErrorCode;
import com.gzh.common.ResultUtils;
import com.gzh.exception.BusinessException;
import com.gzh.model.bo.TeamBO;
import com.gzh.model.domain.Team;
import com.gzh.model.domain.User;
import com.gzh.model.domain.UserTeam;
import com.gzh.model.dto.TeamInfoQuery;
import com.gzh.model.dto.TeamQuery;
import com.gzh.model.request.TeamAddRequest;
import com.gzh.model.request.TeamJoinRequest;
import com.gzh.model.request.TeamQuitRequest;
import com.gzh.model.request.TeamUpdateRequest;
import com.gzh.model.vo.TeamUserVO;
import com.gzh.service.TeamService;
import com.gzh.service.UserService;
import com.gzh.service.UserTeamService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Hal
 * @since 2023/01/30
 */

@Slf4j
@RestController
@RequestMapping("/team")
@CrossOrigin(origins = {"http://localhost:8080","http://localhost:80","http://localhost:8081","http://localhost:5173","http://localhost:3000"}, allowCredentials="true")
public class TeamController {

    @Resource
    private TeamService teamService;
    @Resource
    private UserService userService;

    @Resource
    private UserTeamService userTeamService;

    @PostMapping("/add")
    public BaseResponse<Long> createTeam(@RequestBody TeamAddRequest teamAddRequest, HttpServletRequest request) {
        if (teamAddRequest == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        Team team = new Team();
        BeanUtils.copyProperties(teamAddRequest, team);
        long teamId = teamService.createTeam(team, loginUser);
        return ResultUtils.success(teamId);
    }

    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteTeam(@RequestBody DeleteRequest deleteRequest, HttpServletRequest request) {
        if (deleteRequest == null || deleteRequest.getId() <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean result = teamService.deleteTeam(deleteRequest.getId(),loginUser);
        if (!result) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "删除失败");
        }
        return ResultUtils.success(true);
    }

    @PostMapping("/update")
    public BaseResponse<Boolean> updateTeam(@RequestBody TeamUpdateRequest teamUpdateRequest, HttpServletRequest request) {
        if (teamUpdateRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean result = teamService.updateTeam(teamUpdateRequest, loginUser);
        if (!result) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "更新失败");
        }
        return ResultUtils.success(true);
    }

    @GetMapping("/get")
    public BaseResponse<Team> getTeamById(long id) {
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = teamService.getById(id);
        if (team == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        return ResultUtils.success(team);
    }

    @GetMapping("/list")
    public BaseResponse<List<TeamUserVO>> listTeams(TeamQuery teamQuery, HttpServletRequest request) {
        if (teamQuery == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean isAdmin = userService.isAdmin(request);
        // 查询队伍列表
        List<TeamUserVO> teamList = teamService.listTeams(teamQuery, isAdmin);
        if (CollectionUtils.isEmpty(teamList)) {
            return ResultUtils.success(teamList);
        }
        try {
            // 判断当前用户加入了哪些队伍
            User loginUser = userService.getLoginUser(request);
            userTeamService.setHasJoinTeam(teamList, loginUser.getId());
        } catch (Exception e) {
            // 表示该用户未加入任何队伍 直接跳过try方法执行下一个方法 (这里可以不登录所以手动catch)
            log.debug("未登录调用接口 - 无视异常");
        }
        // 查询队伍已加入的人数
        teamList = userTeamService.setJoinedTeamNum(teamList);
        return ResultUtils.success(teamList);
    }

    @GetMapping("/info")
    public BaseResponse<TeamBO> teamInfo(TeamInfoQuery teamInfoQuery) {
        if (teamInfoQuery == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        // 查出队伍信息
        Team team = teamService.getById(teamInfoQuery.getId());
        if (team == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        TeamBO teamBO = new TeamBO();
        BeanUtils.copyProperties(team, teamBO);
        // 如队伍有人 查出所有用户信息
        List<Long> teamUserIds = teamInfoQuery.getTeamUserIds();
        if (!CollectionUtils.isEmpty(teamUserIds)) {
            LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<>();
            lqw.in(User::getId, teamUserIds);
            List<User> userList = userService.list(lqw);
            // 用户脱敏并存入
            teamBO.setUserList(userList.stream().map(user -> userService.getSafetyUser(user)).collect(Collectors.toList()));
        }
        return ResultUtils.success(teamBO);
    }

    @GetMapping("/list/page")
    public BaseResponse<Page<Team>> listPageTeams(TeamQuery teamQuery) {
        if (teamQuery == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = new Team();
        BeanUtils.copyProperties(teamQuery, team);
        Page<Team> page = new Page<>(teamQuery.getPageNum(), teamQuery.getPageSize());
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>(team);
        Page<Team> resultPage = teamService.page(page, queryWrapper);
        return ResultUtils.success(resultPage);
    }

    @PostMapping("/join")
    public BaseResponse<Boolean> joinTeam(@RequestBody TeamJoinRequest teamJoinRequest, HttpServletRequest request){
        if (teamJoinRequest==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean result = teamService.joinTeam(teamJoinRequest, loginUser);
        return ResultUtils.success(result);
    }

    @PostMapping("/quit")
    public BaseResponse<Boolean> quitTeam(@RequestBody TeamQuitRequest teamQuitRequest, HttpServletRequest request){
        if (teamQuitRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean result = teamService.quitTeam(teamQuitRequest, loginUser);
        return ResultUtils.success(result);
    }

    /**
     * 获取我创建的队伍
     */
    @GetMapping("/list/my/teams")
    public BaseResponse<List<TeamUserVO>> listMyTeams(TeamQuery teamQuery, HttpServletRequest request) {
        if (teamQuery == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        teamQuery.setUserId(loginUser.getId());
        List<TeamUserVO> teamList = teamService.listTeams(teamQuery, true);

        // 查询队伍已加入的人数
        teamList = userTeamService.setJoinedTeamNum(teamList);
        return ResultUtils.success(teamList);
    }

    /**
     * 获取已加入的队伍
     */
    @GetMapping("/list/joined/teams")
    public BaseResponse<List<TeamUserVO>> listJoinedTeams(TeamQuery teamQuery, HttpServletRequest request) {
        if (teamQuery == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);

        // 根据用户id 查出所属队伍列表
        LambdaQueryWrapper<UserTeam> lqw = new LambdaQueryWrapper<>();
        lqw.eq(UserTeam::getUserId, loginUser.getId());
        List<UserTeam> userTeamList = userTeamService.list(lqw);

        if (CollectionUtils.isEmpty(userTeamList)) {
            return ResultUtils.success(Collections.emptyList());
        }

        // 根据队伍id分组 来取出不重复的队伍 id
        Map<Long, List<UserTeam>> listMap = userTeamList.stream()
                .collect(Collectors.groupingBy(UserTeam::getTeamId));
        // 转list
        List<Long> idList = new ArrayList<>(listMap.keySet());
        teamQuery.setIdList(idList);
        List<TeamUserVO> teamList = teamService.listTeams(teamQuery, true);

        // 查询队伍已加入的人数
        teamList = userTeamService.setJoinedTeamNum(teamList);
        return ResultUtils.success(teamList);
    }
}
