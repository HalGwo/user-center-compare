package com.gzh.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 *
 * @author GZH
 * @version 1.0
 * @since 2022/11/12 11:46
 */
@Data
public class UserRegisterRequest implements Serializable {

    private static final long serialVersionUID = -3430966666812783758L;
    
    private String userAccount;
    
    private String userPassword;
    
    private String checkPassword;

    private String planetCode;
}
