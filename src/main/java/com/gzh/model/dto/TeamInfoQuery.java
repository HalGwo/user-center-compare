package com.gzh.model.dto;

import com.gzh.common.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 队伍查询封装类
 *
 * @author Hal
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TeamInfoQuery extends PageRequest {

    private static final long serialVersionUID = -8627530915837224803L;
    /**
     * id
     */
    private Long id;

    /**
     * id 列表
     */
    private List<Long> teamUserIds;
}