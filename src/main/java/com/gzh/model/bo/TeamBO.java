package com.gzh.model.bo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gzh.model.domain.Team;
import com.gzh.model.domain.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 队伍
 *
 * @author Hal
 */
@Data
@TableName(value ="team")
@EqualsAndHashCode(callSuper = true)
public class TeamBO extends Team {

    private static final long serialVersionUID = 5947351138513363621L;

    /** 队伍用户列表 */
    private List<User> userList;
}