package com.gzh.model.bo;

import com.gzh.model.domain.UserTeam;
import lombok.NoArgsConstructor;

/**
 * 用户注册请求体
 *
 * @author GZH
 * @since 2024/1/26
 */
@NoArgsConstructor
public class UserTeamBO extends UserTeam {
    private static final long serialVersionUID = -8943734384886741519L;

    public UserTeamBO(Long teamId, Long userId) {
        super.setTeamId(teamId);
        super.setUserId(userId);
    }
}
