package com.gzh.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 队伍状态枚举
 *
 * @author Hal
 */
@Getter
@AllArgsConstructor
public enum TeamStatusEnum {

    PUBLIC(0, "公开"),
    PRIVATE(1, "私有"),
    SECRET(2, "加密");

    private final int value;

    private final String text;

    public static TeamStatusEnum getEnumByValue(Integer value) {
        if (value == null) {
            return null;
        }
        // 遍历获取枚举值
        TeamStatusEnum[] values = TeamStatusEnum.values();
        for (TeamStatusEnum teamStatusEnum : values) {
            if (teamStatusEnum.getValue() == value) {
                return teamStatusEnum;
            }
        }
        return null;
    }
}