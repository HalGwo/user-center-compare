# export JAVA_HOME=/usr/local/java
# export PATH=$JAVA_HOME/bin;$PATH

export JAVA_HOME=/usr/local/java/jdk1.8.0_333
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH

// 文件如果损坏 etc/profile 复原 命令行输入 export PATH=/usr/bin:/usr/sbin:/bin:/sbin:/usr/X11R6/bin

# nginx
yum install gcc-c++
yum install -y pcre pcre-devel
yum install -y zlib zlib-devel
yum install -y openssl openssl-devel

mkdir /usr/local/nginx
tar -zxvf /tmp/mysoft/nginx.xxx.tar.gz -C /usr/local/nginx
cd /usr/local/nginx/nginx.xxx
./configure --with-http_stub_status_module --with-http_ssl_module
make && make install

#如果没有向java一样配置环境 则用不了nginx命令
#要用./nginx
#./nginx需要进入sbin目录下才能使用
cd /usr/local/nginx/sbin
#启动
./nginx
#查看进程
ps -ef | grep nginx
#停止
./nginx -s stop
#重启
./nginx -s reload


#输入服务器地址直接访问 看到nginx则成功
#没看到去服务器后台开启访问80端口
#阿里云 -> 安全组 -> (管理)规则 -> 添加规则 -> 快速添加 80
vi /usr/local/nginx/conf/nginx.conf
#找到server {listen 80;...}
server {
    listen       80;
    server_name  localhost;
    location / {
        root   /home/server/teamup/vue/dist; # 就只改这里 html; -> /home/server/teamup/vue/dist;
        index  index.html index.htm;
    }




#mysql
#如果是正常版本的mysql需要卸载 如果是.rpm-bundle.tar里面是几个rpm文件则不需要卸载
#卸载
#linux系统会自带一个数据库 需要卸载掉mariadb 版本号不知道怎么看
#查看mariadb版本 无则不显示
rpm -qa | grep mariadb
#mariadb-libs-5.5.60-1.el7_5.x86_64
yum remove -y mariadb-libs-5.5.60-1.el7_5.x86_64
#卸载完成

#解压正常mysql-5.7.37-el7-x86_64.tar.gz
mkdir /data/mysql
tar -zxvf /tmp/mysoft/mysql-5.7.37-el7-x86_64.tar.gz -C /usr/local
#重命名
mv /usr/localmysql-5.7.37-el7-x86_64.tar.gz -C /usr/local/mysql
#添加用户组
groupadd mysql
useradd -r -g mysql mysql
chown -R mysql.mysql /usr/local/mysql
chown -R mysql.mysql /data/mysql

#安装mysql服务
/usr/local/mysql/bin/mysqld --user=mysql --basedir=/usr/local/mysql/ --datadir=/data/mysql --initialize
#报错
error while loading shared libraries: libaio.so.1: cannot open shared object file: No such file or directory
yum install -y libaio.so.1
yum install -y libaio
#成功后
 A temporary password is generated for root@localhost: ,shs%Sf*n9j!
 #密码:
 ,shs%Sf*n9j!

#改配置文件
vi /etc/my.cnf
[mysqld]
datadir=/data/mysql
basedir=/usr/local/mysql
socket=/tmp/mysql.sock
user=mysql
port=3306
character-set-server=utf8
#取消密码验证
#skip-grant-tables
# # Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

#创建mysql快捷方式
ln -s /usr/local/mysql/bin/mysql /usr/bin
ln -s /usr/lib64/libtinfo.so.6.1 /usr/lib64/libtinfo.so.5
ln -s /usr/lib64/libncurses.so.6.1 /usr/lib64/libncurses.so.5

#启动mysql服务
service mysql start
#如果提示service not find错误 运行下面
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql
mysql -uroot -p
#密码之前记住的
#报错
Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' (2)
# https://blog.csdn.net/weixin_42110159/article/details/118945136 原文
#添加快捷方式软连接指向/tmp/mysql.sock 命令行输入↓
ln -s /tmp/mysql.sock /var/lib/mysql/mysql.sock

#成功进入mysql后修改密码
set password=password('root')
#修改权限 不然远程连接不上
#登录进mysql之后
use mysql
update user set host ='%'where user='root' and host ='localhost';
flush privileges;
#阿里云开启端口权限  3306

#启动java
cd /home/server/teamup/java
./start.sh


#解压安装mysql.rpm-bundle
mkdir /data/mysql
mkdir /usr/local/mysql
#这里用的是黑马的mysql MySQL-5.6.22-1.el6.i686.rpm-bundle.tar 不太一样里面是rpm
tar -xvf /tmp/mysoft/MYSQL.xxx.tar.gz -C /usr/local/mysql

#添加用户组
groupadd mysql
useradd -r -g mysql mysql
chown -R mysql.mysql /usr/local/mysql
chown -R mysql.mysql /data/mysql

cd /usr/local/mysql/
#安装server
rpm -ivh MySQL-server-5.6.25-1.el6.x86_64.rpm
#报错 缺少文件
error: Failed dependencies:
	libaio.so.1 is needed by MySQL-server-5.6.22-1.el6.i686
	libaio.so.1(LIBAIO_0.1) is needed by MySQL-server-5.6.22-1.el6.i686
	libaio.so.1(LIBAIO_0.4) is needed by MySQL-server-5.6.22-1.el6.i686
#这里看报错提示 如果是 xxx is needed by mysql
#则表示安装mysql需要xxx
#那就需要安装 输入下面命令 (我这里缺少libaio.so.1 则xxx代表libaio.so.1) 少什么安装什么
yum install xxx
#如果还是继续报错is needed 则继续yum install xxx 一直到rpm -ivh MySQL-server...这条命令执行成功
FATAL ERROR: please install the following Perl modules before executing /usr/bin/mysql_install_db
#安装Perl-Data-Dumper模块
yum install -y perl-Data-Dumper

#安装mysql-client
rpm -ivh MySQL-client-5.6.22-1.el6.i686.rpm
#查看密码
cat /root/.mysql_secret
service mysql start
#登录
mysql -u root -p xxxx
#提示设置密码
set password=password('root')


#创建mysql快捷方式
ln -s /usr/local/mysql/bin/mysql /usr/bin
ln -s /usr/lib64/libtinfo.so.6.1 /usr/lib64/libtinfo.so.5
ln -s /usr/lib64/libncurses.so.6.1 /usr/lib64/libncurses.so.5













res <!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="/vite.svg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="referrer" content="no-referrer" />
    <title>Vite + Vue + TS</title>
    <script type="module" crossorigin src="/assets/index-7xoFnrKx.js"></script>
    <link rel="stylesheet" crossorigin href="/assets/index-wMhqM2Sy.css">
  </head>
  <body>
    <div id="app"></div>
  </body>
</html>



